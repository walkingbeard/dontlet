/*
 * HandlerAttitudeEncoder.h
 *
 *  Created on: 18 Jan 2017
 *      Author: walkingbeard
 * Description:
 */

#ifndef HANDLERATTITUDEENCODER_H_
#define HANDLERATTITUDEENCODER_H_

#include "subscriber.h"
#include "thread.h"
#include "topic.h"
#include "tmTcTopics.h"
#include "components.h"


class HandlerAttitudeEncoder: public RODOS::Thread {
public:
    HandlerAttitudeEncoder();
    void init();
    void run();
};

#endif /* HANDLERATTITUDEENCODER_H_ */
