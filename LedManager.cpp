/*
 * LedManager.cpp
 *
 *  Created on: 2 Jan 2017
 *      Author: walkingbeard
 */

#include "LedManager.h"

void LedManager::switchLED(ledColour col)
{
    switch (col) {
    case GREEN:
        green.setPins(~green.readPins());
        break;
    case BLUE:
        blue.setPins(~blue.readPins());
        break;
    case RED:
        red.setPins(~red.readPins());
        break;
    case ORANGE:
        orange.setPins(~orange.readPins());
        break;
    default:
        break;
    }
}

void LedManager::switchOnLED(ledColour col)
{
    switch (col) {
    case GREEN:
        green.setPins(1);
        break;
    case BLUE:
        blue.setPins(1);
        break;
    case RED:
        red.setPins(1);
        break;
    case ORANGE:
        orange.setPins(1);
        break;
    default:
        break;
    }
}

void LedManager::switchOffLED(ledColour col)
{
    switch (col) {
    case GREEN:
        green.setPins(0);
        break;
    case BLUE:
        blue.setPins(0);
        break;
    case RED:
        red.setPins(0);
        break;
    case ORANGE:
        orange.setPins(0);
        break;
    default:
        break;
    }
}

LedManager::LedManager() :
        green(LED_GREEN), blue(LED_BLUE), red(LED_RED), orange(LED_ORANGE)
{
    green.init(true, 1, 0);
    blue.init(true, 1, 0);
    red.init(true, 1, 0);
    orange.init(true, 1, 0);
}
