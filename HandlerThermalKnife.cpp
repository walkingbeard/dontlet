/*
 * HandlerThermalKnife.cpp
 *
 *  Created on: 6 Jan 2017
 *      Author: walkingbeard
 */

#include "HandlerThermalKnife.h"
#include "components.h"

HandlerThermalKnife::HandlerThermalKnife() :
        Thread("Thermal knife handler"), SubscriberReceiver<TC_ThermalKnife>(
                topicTCThermalKnife, "Thermal knife handler"), timeToRun(0)
{
    // Empty
}

void HandlerThermalKnife::init()
{
    //Empty
}

void HandlerThermalKnife::run()
{
    while (1) {
//        PRINTF("tick\n");
        if (timeToRun != 0) {
            knife.switchOn();
            TTime waitingTime = NOW()+ timeToRun * SECONDS;
            timeToRun = 0;
            suspendCallerUntil(waitingTime);
        } else {
            knife.switchOff();
            suspendCallerUntil(END_OF_TIME);
        }
    }
}

void HandlerThermalKnife::put(TC_ThermalKnife &runtime)
{
//    PRINTF("Received knife command\n");
    if (runtime.value < 0) {
//        PRINTF("Illegal thermal knife time\n");
        return;
    } else if (runtime.value > 30) {
//        PRINTF("Dangerous thermal knife time - disallowed\n");
        return;
    } else {
//        PRINTF("Resuming knifewith value %f\n", runtime.value);
        timeToRun = runtime.value;
        this->resume();
    }
}

