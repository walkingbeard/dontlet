/*
 * Imu.cpp
 *
 *  Created on: 3 Jan 2017
 *      Author: walkingbeard
 */

#include "Imu.h"
#include <cmath>
#include <cstdlib>

void toRadians(SensorVal &data)
{
    data.x = (data.x / 180.0) * M_PI;
    data.y = (data.y / 180.0) * M_PI;
    data.z = (data.z / 180.0) * M_PI;
}


Imu::Imu() :
    chipSelectGyro(GYRO_SELECT_PIN), chipSelectMagAcc(MAGACC_SELECT_PIN),
    enableImu(ENABLE_IMU_PIN), imuI2C(I2C_BUS_NUM), accOffset(ACC_OFFSET_X,
            ACC_OFFSET_Y, ACC_OFFSET_Z), magMin(MAG_MIN_X, MAG_MIN_Y, MAG_MIN_Z),
            magMax(MAG_MAX_X, MAG_MAX_Y, MAG_MAX_Z)
{
    chipSelectGyro.init(true, 1, 1);
    chipSelectMagAcc.init(true, 1, 1);
    enableImu.init(true, 1, 1);
    imuI2C.init(400000);

    init();
}


void Imu::init()
{
    enableImu.setPins(1);
    // TODO: Error reporting mechanism
    initGyro();
    initMagAcc();
}


int8_t Imu::initGyro()
{
    int8_t ret1 = setRegister(GYRO_ADDR, CTRL_REG1_G, CTRL_REG1_G_VAL);
    int8_t ret4 = setRegister(GYRO_ADDR, CTRL_REG4_G, CTRL_REG4_G_VAL);
    return ret1 | ret4;
}


int8_t Imu::initMagAcc()
{
    int8_t ret1 = setRegister(MAGACC_ADDR, CTRL_REG1_XM, CTRL_REG1_XM_VAL);
    int8_t ret2 = setRegister(MAGACC_ADDR, CTRL_REG2_XM, CTRL_REG2_XM_VAL);
    int8_t ret5 = setRegister(MAGACC_ADDR, CTRL_REG5_XM, CTRL_REG5_XM_VAL);
    int8_t ret6 = setRegister(MAGACC_ADDR, CTRL_REG6_XM, CTRL_REG6_XM_VAL);
    int8_t ret7 = setRegister(MAGACC_ADDR, CTRL_REG7_XM, CTRL_REG7_XM_VAL);
    return ret1 | ret2 | ret5 | ret6 | ret7;
}


void Imu::disable()
{
    imuI2C.reset();
    enableImu.setPins(0);
}


int8_t Imu::setRegister(uint8_t dev, uint8_t reg, uint8_t val)
{
    uint8_t data[2] = {reg, val};
    int32_t written = imuI2C.write(dev, data, 2);
    if (written != 2) {
        return -1;
    } else {
        return 0;
    }
}


int8_t Imu::getRegister(uint8_t dev, uint8_t reg, uint8_t &val)
{
    int32_t red = imuI2C.writeRead(dev, &reg, 1, &val, 1);
    if (red != 1) {
        return -1;
    } else {
        return 0;
    }
}


int8_t Imu::readRaw(uint8_t dev, uint8_t startReg, int16_t *data)
{
    uint8_t sensAddr = startReg | 0x80; // Assert MSB for multi-byte read
    uint8_t rawBytes[6] = {0,0,0,0,0,0};
    int16_t rawVals[3] = {0,0,0};

    int32_t red = imuI2C.writeRead(dev, &sensAddr, 1, rawBytes, 6);

    if (red != 6)   // If transfer failed
        return -1;

    for (int i(0); i < 3; i++)
        data[i] = ((int16_t) rawBytes[i * 2 + 1] << 8) | rawBytes[i * 2];

    return 0;
}


int8_t Imu::readGyro(SensorVal &data, bool adjust)
{
    int16_t rawVals[3] = {0,0,0};

    if (readRaw(GYRO_ADDR, OUT_X_L_G, rawVals))
        return -1;

    data.x = rawVals[0];
    data.y = rawVals[1];
    data.z = rawVals[2];

    if (adjust) {
        data = data - gyroOffset;
        data = data * GYRO_SENSITIVITY;
    }

    toRadians(data);

    return 0;
}


int8_t Imu::readMag(SensorVal &data, bool adjust)
{
    int x, y, z;
    int16_t rawVals[3] = {0,0,0};

    if (readRaw(MAGACC_ADDR, OUT_X_L_M, rawVals))
        return -1;

    x = rawVals[0];
    y = rawVals[1];
    z = rawVals[2];

    if (adjust) {
        data.x = (2.0 * ((x - magMin.x)/(magMax.x - magMin.x))) - 1.0;
        data.y = (2.0 * ((y - magMin.y)/(magMax.y - magMin.y))) - 1.0;
        data.z = (2.0 * ((z - magMin.z)/(magMax.z - magMin.z))) - 1.0;
        data = data * MAG_SENSITIVITY;
    } else {
        data.x = x;
        data.y = y;
        data.z = z;
    }

    return 0;
}


int8_t Imu::readAcc(SensorVal &data, bool adjust)
{
    int16_t rawVals[3] = {0,0,0};

    if (readRaw(MAGACC_ADDR, OUT_X_L_A, rawVals))
        return -1;

    data.x = rawVals[0];
    data.y = rawVals[1];
    data.z = rawVals[2];

    if (adjust) {
        data = data - accOffset;
        data = data * ACC_SENSITIVITY;
    }

    return 0;
}


void Imu::setOffsetGyro(const SensorVal &offset)
{
    gyroOffset = offset;
}


void Imu::setOffsetAcc(const SensorVal &offset)
{
    accOffset = offset;
}


void Imu::setOffsetMag(const SensorVal &max, const SensorVal &min)
{
    magMax = max;
    magMin = min;
}


int8_t Imu::checkIdGyro()
{
    uint8_t whoAmI;
    int8_t ret = getRegister(GYRO_ADDR, WHO_AM_I, whoAmI);

    if (ret) {
        return ret;
    } else {
        return whoAmI == WHO_AM_I_G_VAL ? 0 : 1;
    }
}


int8_t Imu::checkIdMagAcc()
{
    uint8_t whoAmI;
    int8_t ret = getRegister(MAGACC_ADDR, WHO_AM_I, whoAmI);

    if (ret) {
        return ret;
    } else {
        return whoAmI == WHO_AM_I_XM_VAL ? 0 : 1;
    }
}


float Imu::getStableHeading(SensorVal acc, SensorVal mag)
{
    float stableHeading;

    acc.y = acc.y - accOffset.y;
    acc.x = acc.x - accOffset.x;
    acc.z = acc.z - accOffset.z - 1000;

    float rollByAcc = atan(acc.y / sqrt((acc.x * acc.x)+(acc.z * acc.z)));
    float pitchByAcc = atan(acc.x / sqrt((acc.y * acc.y)+(acc.z * acc.z)));

    float headingMagX = mag.x * cosf(pitchByAcc) + mag.z * sinf(pitchByAcc);
    float headingMagY = mag.x * sinf(rollByAcc) * sinf(pitchByAcc) + mag.y * cosf(rollByAcc) - mag.z * sinf(rollByAcc) * cosf(pitchByAcc);

    //heading
    float heading = atan(headingMagY / headingMagX);

    //compensation
    if (headingMagX < 0) {
        stableHeading = 3.1415 - heading;
    } else if (headingMagX > 0 && headingMagY < 0) {
        stableHeading = -heading;
    } else if (headingMagX > 0 && headingMagY > 0) {
        stableHeading = (2 * 3.1415) - heading;
    } else if (headingMagX == 0 && headingMagY < 0) {
        stableHeading = (3.1415 / 2);
    } else if (headingMagX == 0 && headingMagY > 0) {
        stableHeading = (3 / 2) * 3.1415;
    }

    stableHeading = stableHeading * 180 / M_PI - 90;

    if(stableHeading > 360){
        stableHeading = stableHeading - 360;
    }
    if(stableHeading < 0){
        stableHeading = stableHeading + 360;
    }

    stableHeading = stableHeading * M_PI / 180;

    return stableHeading;
}


void Imu::printOffsets()
{
    PRINTF("G offset: x=%f y=%f z=%f\nA offset: x=%f y=%f z=%f\nM min=(%f, %f, %f) max=(%f, %f, %f)\n",
           gyroOffset.x, gyroOffset.y, gyroOffset.z, accOffset.x, accOffset.y,
           accOffset.z, magMin.x, magMin.y, magMin.z, magMax.x, magMax.y, magMax.z);
}
