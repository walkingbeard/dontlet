/*
 * HandlerDockingGo.cpp
 *
 *  Created on: 5 Feb 2017
 *      Author: walkingbeard
 */

#include "HandlerDockingGo.h"
#include "components.h"

HandlerDockingGo::HandlerDockingGo() :
        Thread(), SubscriberReceiver<TC_DockingGo>(topicTCDockingGo), 
        timeToRun(false), currentMotion(STOP)
{
    // TODO Auto-generated constructor stub

}

void HandlerDockingGo::put(TC_DockingGo &cmd)
{
    switch (cmd.go) {
    case 1:
        dockingMotor.action(EXTEND);
        currentMotion = EXTEND;
        timeToRun = true;
        this->resume();
        break;
    case 2:
        dockingMagnet.switchOff();
        dockingMotor.action(RETRACT);
        currentMotion = RETRACT;
        timeToRun = true;
        this->resume();
        break;
    default:
        return;
    }
}

void HandlerDockingGo::init()
{

}

void HandlerDockingGo::run()
{
    while (1) {
        if (timeToRun) {
            suspendCallerUntil(NOW()+ DOCK_PERIOD_MS * MILLISECONDS);
            if (currentMotion == EXTEND) {
                dockingMagnet.switchOn();
            }
            currentMotion = STOP;
            dockingMotor.action(STOP);
            timeToRun = 0;
        }
        suspendCallerUntil(NOW() + 115 * MILLISECONDS);
    }
}
