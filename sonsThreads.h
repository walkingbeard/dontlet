/*
 * sonsThreads.h
 *
 *  Created on: 5 Jan 2017
 *      Author: walkingbeard
 */

#ifndef SONSTHREADS_H_
#define SONSTHREADS_H_

#include "HandlerAttitudeMotor.h"
#include "HandlerThermalKnife.h"
#include "HandlerImu.h"
#include "HandlerLightSensor.h"
#include "ErrorReporter.h"
#include "HandlerDockingGo.h"
#include "HandlerJoystick.h"
#include "HandlerAttitudeEncoder.h"

extern HandlerAttitudeMotor handlerAttitude;
extern HandlerThermalKnife  handlerKnife;
extern HandlerImu           handlerImu;
//extern ErrorReporter      errorReporter;
extern HandlerLightSensor   handlerLightSensor;
extern HandlerDockingGo     handlerDocking;
extern HandlerJoystick      handlerJoystick;
extern HandlerAttitudeEncoder handlerEncoder;

#endif /* SONSTHREADS_H_ */
