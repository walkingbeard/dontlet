/*
 * requiredInitializations.cpp
 *
 *  Created on: Nov 15, 2016
 *      Author: root
 */

#include <rodos.h>
#include "hal.h"
#include "stdio.h"
#include "math.h"
#include "wifitopics.h"
#include "../components.h"

class GatewayInitiator: public Initiator {

    void init()
    {
        gw.resetTopicsToForward();
        gw.addTopicsToForward(&topicTMInertialMeasurements);
        gw.addTopicsToForward(&topicTMLightSensor);
        gw.addTopicsToForward(&topicTMBatteryPower);
        gw.addTopicsToForward(&topicTMSolarPower);
        gw.addTopicsToForward(&topicTMSTMImage);
        gw.addTopicsToForward(&topicTMThermalKnife);
        gw.addTopicsToForward(&topicTMPIDValues);
        gw.addTopicsToForward(&topicTMMotor);
        gw.addTopicsToForward(&topicTCThermalKnife);
        gw.addTopicsToForward(&topicTCPIDValues);
        gw.addTopicsToForward(&topicTCMotor);
        gw.addTopicsToForward(&topicTCJoystickValues);
        gw.addTopicsToForward(&topicTCDockingGo);
        gw.addTopicsToForward(&topicTCAngleRotation);
        gw.addTopicsToForward(&topicTCVelocityRotation);
    }
} gatewayInitiator;

class SystemInitialization: public Thread {
public:

    void init()
    {
        // Initializations of the UART (uart port and the baudrate)
        // Initializations of the I2C and SPI interfaces.
        //	.... etc
        //  	.... etc
    }

    void run()
    {

        init();
        /*** Gateway Wifi ********/
        leds.switchOnLED(BLUE);
        wf121.init("YETENet", "yeteyete");
        // Target IP Address: 192.168.0.102 = 0xC0A80066 (reverse and hex)
        // Target Port: 2000
        //	  Liv 0xC0A801C3
        wf121.enableUDPConnection(0xDD01A8C0, 2002);    // Groundstation
//        wf121.enableUDPConnection(0x6E01A8C0, 2002);    // Jack's laptop
//        wf121.enableUDPConnection(0xFF01A8C0, 2002);
        /**************************/
        leds.switchOffLED(BLUE);
        PRINTF("Hi Guys From FloatSat Program");
    }
} systemInitialization;
