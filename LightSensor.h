/*
 * LightSensor.h
 *
 *  Created on: 31 Jan 2017
 *      Author: walkingbeard
 */

#ifndef LIGHTSENSOR_H_
#define LIGHTSENSOR_H_

#include <cstdint>
#include "rodos.h"
#include "hal.h"

// I2C bus number
#define LS_I2C_BUS_NUM I2C_IDX1

// Addresses
#define LS_I2C_ADDR  0x39
#define LS_DATA0LOW  0xC
#define LS_DATA0HIGH 0xD
#define LS_DATA1LOW  0xE
#define LS_DATA1HIGH 0xF
#define LS_CONTROL   0x0
#define LS_TIMING    0x1
#define LS_ID        0xA

// Register values
// 1-Req, 1-unused, 1-Word R/W off, 1-Block R/W off, 4 reg addr (use or)
#define LS_COMMAND_VAL  0b10000000
#define LS_CONTROL_UP   1
#define LS_CONTROL_DOWN 0
// 3-unused, 1-lo gain, 2-unused, 2-13.7ms integration time
#define LS_TIMING_VAL   0b00000000
// HO 4b 0001 for TSL2561, LO nibble unused
#define LS_ID_REQD      0b00010000


class LightSensor {
private:
    uint16_t visReading;
    uint16_t irReading;
    uint16_t   lux;
    RODOS::HAL_I2C lsI2C;

    // The TSL2561 has a visible/IR sensor and a dedicated IR sensor. Both are
    // used to calculated the lux incident on the sensor.
    int8_t readVis();
    int8_t readIR();
public:
    LightSensor();
    ~LightSensor();
    int8_t init();
    int8_t switchOff();
    int8_t read(uint16_t &outLux);
    int8_t idCheck();
};

#endif /* LIGHTSENSOR_H_ */
