
#include "rodos.h"
#include "wifitopics.h"


// Topic<Telemetry1> telemetry1(5661,"Message1");
// Topic<Telemetry2> telemetry2(5771,"Message2");
// Topic<Telecommand> telecommand(300,"Message2");

/*
HAL_UART gatewayUART(UART_IDX3);
LinkinterfaceUART linkif(&gatewayUART);
Gateway gw(&linkif,true);*/

HAL_UART gatewayWifi(UART_IDX3); // USB-UART
WF121 wf121(&gatewayWifi);
LinkinterfaceWF121 linkwf121(&wf121);
Gateway gw(&linkwf121,true);
