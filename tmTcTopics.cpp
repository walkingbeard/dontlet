/*
 * tmTcTopics.cpp
 *
 *  Created on: 22 Dec 2016
 *      Author: walkingbeard
 *
 * Contains structures for passing of telecommands and telemetry between the
 * satellite and the groundstation. These are defined by the cross-team
 * ground station devs.
 */

#include "rodos.h"
#include "topic.h"
#include "tmTcTopics.h"

RODOS::Topic<TM_InertialMeasurements> topicTMInertialMeasurements(901,
        "Inertial measurements TM");
RODOS::Topic<TM_LightSensor> topicTMLightSensor(902, "Light sensor TM");
RODOS::Topic<TM_BatteryPower> topicTMBatteryPower(903, "Battery power TM");
RODOS::Topic<TM_SolarPower> topicTMSolarPower(904, "Solar power TM");
RODOS::Topic<TM_STMImage> topicTMSTMImage(905, "STM image TM");
RODOS::Topic<TM_ThermalKnife> topicTMThermalKnife(906, "Thermal knife TM");
RODOS::Topic<TM_PIDValues> topicTMPIDValues(907, "PID controller values TM");
RODOS::Topic<TM_Motor> topicTMMotor(908, "Motor TM");
RODOS::Topic<TM_Connection> topicConnection(909,
    "Connection something or other");

RODOS::Topic<TC_ThermalKnife> topicTCThermalKnife(1001, "Thermal knife TC");
RODOS::Topic<TC_PIDValues> topicTCPIDValues(1002, "PID controller values TC");
RODOS::Topic<TC_Motor> topicTCMotor(1003, "Motor TC");
RODOS::Topic<TC_JoystickValues> topicTCJoystickValues(1004,
        "Joystick values TC");
RODOS::Topic<TC_DockingGo> topicTCDockingGo(1005, "Docking GO! GO! GO!");
RODOS::Topic<TC_AngleRotation> topicTCAngleRotation(1006, "Angle rotation TC");
RODOS::Topic<TC_VelocityRotation> topicTCVelocityRotation(1007,
        "Velocity rotation TC");
RODOS::Topic<TC_STMImage> topicTCSTMImage(1008, "STM image TC");


RODOS::Topic<char *> topicErrorReports(1019, "Error reports"); // Via serial
RODOS::Topic<TC_ImuCalib> topicTCImuCalibration(1020,
        "IMU calibration commands");
RODOS::Topic<char *> topicReports(1021, "Telemetry reports"); // Via wireless
