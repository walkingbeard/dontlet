/*
 * LightSensor.cpp
 *
 *  Created on: 31 Jan 2017
 *      Author: walkingbeard
 */

#include "LightSensor.h"
#include <hal_i2c.h>

// Scaling coefficients from datasheet !THESE ARE NOT FULLY EXPLAINED!

#define K1C 0x0043 // 0.130 * 2^RATIO_SCALE(9)
#define B1C 0x0204 // 0.0315 * 2^LUX_SCALE(14)
#define M1C 0x01ad // 0.0262 * 2^LUX_SCALE
#define K2C 0x0085 // 0.260 * 2^RATIO_SCALE
#define B2C 0x0228 // 0.0337 * 2^LUX_SCALE
#define M2C 0x02c1 // 0.0430 * 2^LUX_SCALE
#define K3C 0x00c8 // 0.390 * 2^RATIO_SCALE
#define B3C 0x0253 // 0.0363 * 2^LUX_SCALE
#define M3C 0x0363 // 0.0529 * 2^LUX_SCALE
#define K4C 0x010a // 0.520 * 2^RATIO_SCALE
#define B4C 0x0282 // 0.0392 * 2^LUX_SCALE
#define M4C 0x03df // 0.0605 * 2^LUX_SCALE
#define K5C 0x014d // 0.65 * 2^RATIO_SCALE
#define B5C 0x0177 // 0.0229 * 2^LUX_SCALE
#define M5C 0x01dd // 0.0291 * 2^LUX_SCALE
#define K6C 0x019a // 0.80 * 2^RATIO_SCALE
#define B6C 0x0101 // 0.0157 * 2^LUX_SCALE
#define M6C 0x0127 // 0.0180 * 2^LUX_SCALE
#define K7C 0x029a // 1.3 * 2^RATIO_SCALE
#define B7C 0x0037 // 0.00338 * 2^LUX_SCALE
#define M7C 0x002b // 0.00260 * 2^LUX_SCALE
#define K8C 0x029a // 1.3 * 2^RATIO_SCALE
#define B8C 0x0000 // 0.000 * 2^LUX_SCALE
#define M8C 0x0000 // 0.000 * 2^LUX_SCALE


LightSensor::LightSensor() : lsI2C(LS_I2C_BUS_NUM), visReading(0), irReading(0),
                             lux(0.0)
{
    lsI2C.init();
}


LightSensor::~LightSensor()
{
    switchOff();
}


int8_t LightSensor::init()
{
    uint8_t addr(LS_I2C_ADDR);
    uint8_t cmdStr[2];

    lsI2C.init();

    // Switch on
    cmdStr[0] = LS_CONTROL;  // Access CONTROL register
    cmdStr[1] = LS_CONTROL_UP;

    if (lsI2C.write(addr, cmdStr, 2) != 2)
    {
        return -1;
    }

    // Set up
    cmdStr[0] = LS_COMMAND_VAL | LS_TIMING; // Access timing register
    cmdStr[1] = LS_TIMING_VAL;
    if (lsI2C.write(addr, cmdStr, 2) != 2)
    {
        return -1;
    }

    return 0;
}


int8_t LightSensor::switchOff()
{
    uint8_t addr(LS_I2C_ADDR);
    uint8_t cmdStr[2];

    cmdStr[0] = LS_COMMAND_VAL | LS_CONTROL;  // Access CONTROL register
    cmdStr[1] = LS_CONTROL_DOWN;

    if (lsI2C.write(addr, cmdStr, 2) != 2)
    {
        return -1;
    } else {
        return 0;
    }
}


int8_t LightSensor::readVis()
{
    uint8_t addr(LS_I2C_ADDR);
    uint8_t cmd;
    uint8_t lo;
    uint8_t hi;

    cmd = LS_COMMAND_VAL | LS_DATA0LOW;

    if (lsI2C.writeRead(addr, &cmd, 1, &lo, 1) != 1)
    {
        return -1;
    }

    cmd = LS_COMMAND_VAL | LS_DATA0HIGH;

    if (lsI2C.writeRead(addr, &cmd, 1, &hi, 1) != 1)
    {
        return -1;
    }

    visReading = (hi << 8) | lo;

    return 0;
}


int8_t LightSensor::readIR()
{
    uint8_t addr(LS_I2C_ADDR);
    uint8_t cmd;
    uint8_t lo;
    uint8_t hi;

    cmd = LS_COMMAND_VAL | LS_DATA0LOW;

    if (lsI2C.writeRead(addr, &cmd, 1, &lo, 1) != 1)
    {
        return -1;
    }

    cmd = LS_COMMAND_VAL | LS_DATA0HIGH;

    if (lsI2C.writeRead(addr, &cmd, 1, &hi, 1) != 1)
    {
        return -1;
    }

    irReading = (hi << 8) | lo;

    return 0;
}


int8_t LightSensor::read(uint16_t &outLux)
{
    // Don't pretend to understand this code
    if (readIR() || readVis()) {
        return -1;
    }

    int64_t chanScale = (0x7517) << 4; // From data sheet
    int64_t chan0 = (visReading * chanScale) >> 10;
    int64_t chan1 = (irReading * chanScale) >> 10;
    
    int64_t ratio = (chan0 == 0) ? 0 : (((chan1 << 10) / chan0) + 1) >> 1;

    uint32_t b, m;  // Unexplained names

    if ((ratio >= 0) && (ratio <= K1C)) {
        b=B1C; m=M1C;
    } else if (ratio <= K2C) {
        b=B2C; m=M2C;
    } else if (ratio <= K3C) {
        b=B3C; m=M3C;
    } else if (ratio <= K4C) {
        b=B4C; m=M4C;
    } else if (ratio <= K5C) {
        b=B5C; m=M5C;
    } else if (ratio <= K6C) {
        b=B6C; m=M6C;
    } else if (ratio <= K7C) {
        b=B7C; m=M7C;
    } else if (ratio > K8C) {
        b=B8C; m=M8C;
    }

    uint64_t temp = chan0 * b - chan1 * m;
    if (temp < 0) temp = 0;

    temp += (1 << 13);

    lux = temp >> 14;

    outLux = lux;

    return 0;
}

int8_t LightSensor::idCheck()
{
    return -1;
}
