/*
 * SensorVal.h
 *
 * Created on: 2 Jan 2017
 *      Author: walkingbeard
 *
 * Description: A simple 3-vector for use in retrieving data from the IMU
 */

#ifndef SENSORVAL_H_
#define SENSORVAL_H_

// Holds sensor values
class SensorVal {
public:
    double x;
    double y;
    double z;

    SensorVal();
    SensorVal(double _x, double _y, double _z);

    // Adds two vectors
    friend SensorVal operator+(const SensorVal &left, const SensorVal &right);

    // Subtracts one vector from another
    friend SensorVal operator-(const SensorVal &left, const SensorVal &right);

    // Divides a vector LHS by a scalar RHS
    friend SensorVal operator/(const SensorVal &left, const double &right);

    // Multiplies a vector LHS by a scalar RHS
    friend SensorVal operator*(const SensorVal &left, const double &right);
};

#endif /* SENSORVAL_H_ */
