/*
 * HandlerDockingGo.h
 *
 *  Created on: 5 Feb 2017
 *      Author: walkingbeard
 */

#ifndef HANDLERDOCKINGGO_H_
#define HANDLERDOCKINGGO_H_

#include "subscriber.h"
#include "thread.h"
#include "topic.h"
#include "tmTcTopics.h"
#include "DockingMotor.h"

#define DOCK_PERIOD_MS 2000


class HandlerDockingGo: public RODOS::SubscriberReceiver<TC_DockingGo>,
                        public RODOS::Thread {
private:
    bool          timeToRun;
    DockingMotion currentMotion;

public:
    HandlerDockingGo();
    void put(TC_DockingGo &cmd);
    void init();
    void run();
};

#endif /* HANDLERDOCKINGGO_H_ */
