/*
 * components.cpp
 *
 *  Created on: 5 Jan 2017
 *      Author: walkingbeard
 */

#include "components.h"
#include "rodos.h"

TTime         tmInterval(DEFAULT_GLOBAL_TM_INTERVAL * MILLISECONDS);

LedManager     leds;
AttitudeMotor  attitudeMotor;
Imu            imu;
ThermalKnife   knife;
//LightSensor   lightSensor;
LIGHT          lightSensor;
DockingMotor   dockingMotor;
DockingMagnet  dockingMagnet;
MadgwickFilter madgwick;
