/*
 * AttitudeMotor.h
 *
 *  Created on: 3 Jan 2017
 *      Author: walkingbeard
 *
 * Description: Handles the running of a brushed DC motor for use in attitude
 *              control
 */

#ifndef ATTITUDEMOTOR_H_
#define ATTITUDEMOTOR_H_

#include "rodos.h"
#include "hal.h"

// Using PWM timer 4 channel 1
#define PWM_MOTOR_INDEX     PWM_IDX13

// Using 1000Hz frequency with 100 increments
#define ATM_PWM_FREQ    1000
#define ATM_PWM_INC     100

class AttitudeMotor {
private:
    HAL_GPIO hBridgeInA;
    HAL_GPIO hBridgeInB;
    HAL_PWM motor;
public:
    AttitudeMotor();
    void setSpeed(int16_t speed);
};

#endif /* ATTITUDEMOTOR_H_ */
