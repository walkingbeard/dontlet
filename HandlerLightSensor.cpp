/*
 * HandlerLightSensor.cpp
 *
 *  Created on: 1 Feb 2017
 *      Author: walkingbeard
 */

#include "HandlerLightSensor.h"
#include "components.h"
#include "tmTcTopics.h"

HandlerLightSensor::HandlerLightSensor() : RODOS::Thread("Light sensor")
{
    // TODO Auto-generated constructor stub

}

void HandlerLightSensor::init()
{
//    if (lightSensor.init()) {
//        //leds.switchOnLED(RED);
//        TM_LightSensor lux;
//        lux.value = 34;
//        topicTMLightSensor.publish(lux);
//    }

}

void HandlerLightSensor::run()
{
    lightSensor.init();
    while (1) {
//        uint16_t luxVal(3);
//        TM_LightSensor lux;
//        lightSensor.read(luxVal);
//        lux.value = (float) luxVal;
//        topicTMLightSensor.publish(lux);

        TM_LightSensor lux;
        float newVal;

        lightSensor.readLight(&newVal);
        lux.value = newVal;
        topicTMLightSensor.publish(lux);
        PRINTF("Lux: %f\n", lux);
        suspendCallerUntil(NOW() + tmInterval);
    }
}
