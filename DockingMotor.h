/*
 * DockingMotor.h
 *
 *  Created on: 5 Feb 2017
 *      Author: walkingbeard
 */

#ifndef DOCKINGMOTOR_H_
#define DOCKINGMOTOR_H_

#include "rodos.h"
#include "hal.h"

#define DOCK_PWM_MOTOR_INDEX PWM_IDX12

// Using 1000Hz frequency with 100 increments
#define DOCK_PWM_FREQ 1000
#define DOCK_PWM_INC  100

#define DOCK_SPEED 70


enum DockingMotion {
    EXTEND,
    RETRACT,
    STOP
};

class DockingMotor {
private:
    HAL_GPIO hBridgeInA;
    HAL_GPIO hBridgeInB;
    HAL_PWM  motor;

public:
    DockingMotor();
    void action(DockingMotion motion);
};

#endif /* DOCKINGMOTOR_H_ */
