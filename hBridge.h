/*
 * hBridge.h
 *
 *  Created on: 3 Jan 2017
 *      Author: walkingbeard
 * Description: Management of the H-Bridge interfaces
 */

#ifndef HBRIDGE_H_
#define HBRIDGE_H_

#include "rodos.h"

#define PIN_HBRIDGE_ENABLE GPIO_066

#define HBRIDGE_C_PWM_INDEX PWM_IDX14

// H-Bridge input pin numbers
#define HBRIDGE_A_INA_GPIO  GPIO_036
#define HBRIDGE_A_INB_GPIO  GPIO_017
#define HBRIDGE_B_INA_GPIO  GPIO_016
#define HBRIDGE_B_INB_GPIO  GPIO_071
#define HBRIDGE_C_INA_GPIO  GPIO_072
#define HBRIDGE_C_INB_GPIO  GPIO_074
#define HBRIDGE_D_INA_GPIO  GPIO_076
#define HBRIDGE_D_INB_GPIO  GPIO_079

extern void enableHBridges();

#endif /* HBRIDGE_H_ */
