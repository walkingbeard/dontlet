/*
 * ErrorReporter.h
 *
 *  Created on: 6 Jan 2017
 *      Author: walkingbeard
 * Description: Listens to a general error reporting topic and relays the errors on to
 *              UART3 for serial output.
 */

#ifndef ERRORREPORTER_H_
#define ERRORREPORTER_H_

#include "rodos.h"
#include <subscriber.h>
#include "tmTcTopics.h"

class ErrorReporter: public RODOS::SubscriberReceiver<char *> {
public:
    ErrorReporter();
    void put(char * &msg);
};

#endif /* ERRORREPORTER_H_ */
