/*
 * HandlerImu.cpp
 *
 *  Created on: 6 Jan 2017
 *      Author: walkingbeard
 */

#include "HandlerImu.h"
#include "tmTcTopics.h"
#include "components.h"

// =============================================================================
// Auxiliary functions
// =============================================================================

double minReading(SensorVal magVals[], uint16_t size, Axis ax)
{
    if (size == 0 || ax == FINAL) {
        return 0.0;
    } else if (size == 1) {
        switch (ax) {
        case X:
            return magVals[0].x;
            break;
        case Y:
            return magVals[0].y;
            break;
        case Z:
            return magVals[0].z;
            break;
        }
    } else {
        switch (ax) {
        case X: {
            double min(magVals[0].x);
            for (int i(1); i < size; i++) {
                if (magVals[i].x < min)
                    min = magVals[i].x;
            }
            return min;
            break;
        }
        case Y: {
            double min(magVals[0].y);
            for (int i(1); i < size; i++) {
                if (magVals[i].y < min)
                    min = magVals[i].y;
            }
            return min;
            break;
        }
        case Z: {
            double min(magVals[0].z);
            for (int i(1); i < size; i++) {
                if (magVals[i].z < min)
                    min = magVals[i].z;
            }
            return min;
            break;
        }
        }
    }
}

double maxReading(SensorVal magVals[], uint16_t size, Axis ax)
{
    if (size == 0 || ax == FINAL) {
        return 0.0;
    } else if (size == 1) {
        switch (ax) {
        case X:
            return magVals[0].x;
            break;
        case Y:
            return magVals[0].y;
            break;
        case Z:
            return magVals[0].z;
            break;
        }
    } else {
        switch (ax) {
        case X: {
            double max(magVals[0].x);
            for (int i(1); i < size; i++) {
                if (magVals[i].x > max)
                    max = magVals[i].x;
            }
            return max;
            break;
        }
        case Y: {
            double max(magVals[0].y);
            for (int i(1); i < size; i++) {
                if (magVals[i].y > max)
                    max = magVals[i].y;
            }
            return max;
            break;
        }
        case Z: {
            double max(magVals[0].z);
            for (int i(1); i < size; i++) {
                if (magVals[i].z > max)
                    max = magVals[i].z;
            }
            return max;
            break;
        }
        }
    }
}

SensorVal sumReadings(SensorVal vals[], uint16_t size)
{
    SensorVal sum;
    for (uint16_t i(0); i < size; i++) {
        sum = sum + vals[i];
    }
    return sum;
}

void flushReadings(SensorVal *vals, uint16_t size)
{
    for (int i(0); i < size; i++) {
        vals[i].x = 0;
        vals[i].y = 0;
        vals[i].z = 0;
    }
}

HandlerImu::HandlerImu() :
        Thread("Imu handler"), SubscriberReceiver<TC_ImuCalib>(
                topicTCImuCalibration, "Imu handler"), regime(TELEMETRY), sampleNum(
                0), calibPeriod(
                CALIB_PERIOD_DEFAULT)
{
    // TODO Auto-generated constructor stub
}

void HandlerImu::init()
{

}

void HandlerImu::run()
{
    while (1) {
        switch (regime) {
        case CALIB_GYRO:
            sampleNum--;
            if (!imu.readGyro(sample[sampleNum], false)) {
                char *e = "Error reading gyro for calibration";
                topicErrorReports.publish(e, false);
            }
            if (sampleNum == 0) { // Tie up loose ends and prep for next calibration
                gyroOffset = sumReadings(sample,
                        CALIB_SAMP_NUM) / CALIB_SAMP_NUM;
                imu.setOffsetGyro(gyroOffset);
                gyroOffset.x = gyroOffset.y = gyroOffset.z = 0;
                regime = TELEMETRY;
                PRINTF("Calibration finished\n");
            }
            suspendCallerUntil(NOW()+ calibPeriod * MILLISECONDS);
            break;

            case CALIB_MAG_X:
            sampleNum--;
            if (!imu.readMag(sample[sampleNum], false)) {
                char *e = "Error reading magnetometer for calibration about X";
                topicErrorReports.publish(e, false);
            }
            if (sampleNum == 0) { // Tie up loose ends and prep for next calibration
                magMin.x = minReading(sample, CALIB_SAMP_NUM, X);
                magMax.x = maxReading(sample, CALIB_SAMP_NUM, X);
                regime = TELEMETRY;
                PRINTF("Calibration finished\n");
            }
            suspendCallerUntil(NOW() + calibPeriod * MILLISECONDS);
            break;

            case CALIB_MAG_Y:
            sampleNum--;
            if (!imu.readMag(sample[sampleNum], false)) {
                char *e = "Error reading magnetometer for calibration about Y";
                topicErrorReports.publish(e, false);
            }
            if (sampleNum == 0) { // Tie up loose ends and prep for next calibration
                magMin.y = minReading(sample, CALIB_SAMP_NUM, Y);
                magMax.y = maxReading(sample, CALIB_SAMP_NUM, Y);
                regime = TELEMETRY;
                PRINTF("Calibration finished\n");
            }
            suspendCallerUntil(NOW() + calibPeriod * MILLISECONDS);
            break;

            case CALIB_MAG_Z:
            sampleNum--;
            if (!imu.readMag(sample[sampleNum], false)) {
                char *e = "Error reading magnetometer for calibration about Z";
                topicErrorReports.publish(e, false);
            }
            if (sampleNum == 0) { // Tie up loose ends and prep for next calibration
                magMin.z = minReading(sample, CALIB_SAMP_NUM, Z);
                magMax.z = maxReading(sample, CALIB_SAMP_NUM, Z);
                regime = TELEMETRY;
                PRINTF("Calibration finished\n");
            }
            suspendCallerUntil(NOW() + calibPeriod * MILLISECONDS);
            break;

            case CALIB_MAG_FINAL:
            imu.setOffsetMag(magMax, magMin);
            magMax = magMin = SensorVal();
            PRINTF("Calibration finished\n");
            regime = TELEMETRY;
            suspendCallerUntil(NOW() + calibPeriod * MILLISECONDS);
            break;

            case CALIB_ACC_X:
            sampleNum--;
            if (!imu.readAcc(sample[sampleNum], false)) {
                char *e = "Error reading accelerometer for calibration about X";
                topicErrorReports.publish(e, false);
            }
            if (sampleNum == 0) { // Tie up loose ends and prep for next calibration
                accnCalibValsX = sumReadings(sample, CALIB_SAMP_NUM)
                / CALIB_SAMP_NUM;
                regime = TELEMETRY;
                PRINTF("Calibration finished\n");
            }
            suspendCallerUntil(NOW() + calibPeriod * MILLISECONDS);
            break;

            case CALIB_ACC_Y:
            sampleNum--;
            if (!imu.readAcc(sample[sampleNum], false)) {
                char *e = "Error reading accelerometer for calibration about Y";
                topicErrorReports.publish(e, false);
            }
            if (sampleNum == 0) { // Tie up loose ends and prep for next calibration
                accnCalibValsY = sumReadings(sample, CALIB_SAMP_NUM)
                / CALIB_SAMP_NUM;
                regime = TELEMETRY;
                PRINTF("Calibration finished\n");
            }
            suspendCallerUntil(NOW() + calibPeriod * MILLISECONDS);
            break;

            case CALIB_ACC_Z:
            sampleNum--;
            if (!imu.readAcc(sample[sampleNum], false)) {
                char *e = "Error reading accelerometer for calibration about Z";
                topicErrorReports.publish(e, false);
            }
            if (sampleNum == 0) { // Tie up loose ends and prep for next calibration
                accnCalibValsZ = sumReadings(sample, CALIB_SAMP_NUM)
                / CALIB_SAMP_NUM;
                regime = TELEMETRY;
                PRINTF("Calibration finished\n");
            }
            suspendCallerUntil(NOW() + calibPeriod * MILLISECONDS);
            break;

            case CALIB_ACC_FINAL:
            accnOffset.x = (accnCalibValsZ.x + accnCalibValsY.x) /2;
            accnOffset.y = (accnCalibValsZ.y + accnCalibValsX.y) /2;
            accnOffset.z = ((accnCalibValsX.z + accnCalibValsY.z) /2) - 1000;
            imu.setOffsetAcc(accnOffset);
            accnOffset = SensorVal();
            regime = TELEMETRY;
            PRINTF("Calibration finished\n");
            suspendCallerUntil(NOW() + calibPeriod * MILLISECONDS);
            break;

            case TELEMETRY:
            default:

            SensorVal gyroVal;
            SensorVal rawAccn;
            SensorVal rawMagn;
            TM_InertialMeasurements tempTM;
            
            imu.readGyro(gyroVal, true);
            imu.readAcc(rawAccn, true);
            imu.readMag(rawMagn, true);

            madgwick.filterUpdate(gyroVal.x, gyroVal.y, gyroVal.z, rawAccn.x, rawAccn.y, rawAccn.z, rawAccn.x, rawAccn.y, rawAccn.z);
            SensorVal rollAndPitch = madgwick.getEulerAnglesRad();

            tempTM.pitch_velocity = gyroVal.y;
            tempTM.roll_velocity = gyroVal.x;
            tempTM.yaw_velocity = gyroVal.z;
            tempTM.yaw = imu.getStableHeading(rawAccn, rawMagn);
            tempTM.roll = rollAndPitch.x;
            tempTM.pitch = rollAndPitch.y;
            topicTMInertialMeasurements.publish(tempTM, true);
            
            PRINTF("Roll: %f Pitch: %f\n", tempTM.roll, tempTM.pitch);
            PRINTF("Heading: %f\n", tempTM.yaw);
//            PRINTF("Gyro: %f %f %f\n", gyroVal.x, gyroVal.y, gyroVal.z);
//            PRINTF("Mag: %f %f %f\n", rawMagn.x, rawMagn.y, rawMagn.z);
//            PRINTF("Accn: %f %f %f\n", rawAccn.x, rawAccn.y, rawAccn.z);
//            imu.printOffsets();
            suspendCallerUntil(NOW() + tmInterval);
        }
    }
}

void HandlerImu::put(TC_ImuCalib &command)
{
    switch (command.sensor) {
    case GYROSCOPE:
        regime = CALIB_GYRO;
        sampleNum = CALIB_SAMP_NUM;
        flushReadings(sample, CALIB_SAMP_NUM);
        break;
    case MAGNETOMETER:
        switch (command.axis) {
        case X:
            regime = CALIB_MAG_X;
            sampleNum = CALIB_SAMP_NUM;
            flushReadings(sample, CALIB_SAMP_NUM);
            break;
        case Y:
            regime = CALIB_MAG_Y;
            sampleNum = CALIB_SAMP_NUM;
            flushReadings(sample, CALIB_SAMP_NUM);
            break;
        case Z:
            regime = CALIB_MAG_Z;
            sampleNum = CALIB_SAMP_NUM;
            flushReadings(sample, CALIB_SAMP_NUM);
            break;
        case FINAL:
            regime = CALIB_MAG_FINAL;
            break;
        }
        break;
    case ACCELEROMETER:
        switch (command.axis) {
        case X:
            regime = CALIB_ACC_X;
            sampleNum = CALIB_SAMP_NUM;
            flushReadings(sample, CALIB_SAMP_NUM);
            break;
        case Y:
            regime = CALIB_ACC_Y;
            sampleNum = CALIB_SAMP_NUM;
            flushReadings(sample, CALIB_SAMP_NUM);
            break;
        case Z:
            regime = CALIB_ACC_Z;
            sampleNum = CALIB_SAMP_NUM;
            flushReadings(sample, CALIB_SAMP_NUM);
            break;
        case FINAL:
            regime = CALIB_ACC_FINAL;
            break;
        }
        break;
    }
    resume();
}

