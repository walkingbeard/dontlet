/*
 * ThermalKnife.h
 *
 *  Created on: 3 Jan 2017
 *      Author: walkingbeard
 *
 * Description: Operation of a resistor to cut ties on the solar panels
 */

#ifndef THERMALKNIFE_H_
#define THERMALKNIFE_H_

#include "rodos.h"
#include "hal.h"

#define PWM_MODULATOR_INDEX PWM_IDX15

// Using 1000Hz frequency with one increment
#define TH_PWM_FREQ    1000
#define TH_PWM_INC     1

class ThermalKnife {
private:
    HAL_GPIO hBridgeInA;
    HAL_PWM modulator;

public:
    ThermalKnife();
    void toggle();
    void switchOn();
    void switchOff();
};

#endif /* THERMALKNIFE_H_ */
