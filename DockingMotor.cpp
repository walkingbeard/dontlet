/*
 * DockingMotor.cpp
 *
 *  Created on: 5 Feb 2017
 *      Author: walkingbeard
 */

#include "DockingMotor.h"
#include "hBridge.h"

DockingMotor::DockingMotor() :
        hBridgeInA(HBRIDGE_A_INA_GPIO), hBridgeInB(HBRIDGE_A_INB_GPIO), motor(
                DOCK_PWM_MOTOR_INDEX)
{
    motor.init(DOCK_PWM_FREQ, DOCK_PWM_INC);
    hBridgeInA.init(true, 1, 0);    // Is output, 1 pin, init high
    hBridgeInB.init(true, 1, 0);    // Is output, 1 pin, init low
    enableHBridges();
    motor.write(DOCK_SPEED); // Start motor at halt
}

void DockingMotor::action(DockingMotion motion)
{
    switch (motion) {
    case EXTEND:
        hBridgeInA.setPins(1);
        hBridgeInB.setPins(0);
        break;
    case RETRACT:
        hBridgeInA.setPins(0);
        hBridgeInB.setPins(1);
        break;
    case STOP:
        hBridgeInA.setPins(0);
        hBridgeInB.setPins(0);
        break;
    }
}
