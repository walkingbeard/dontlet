/*
 * HandlerAttitudeEncoder.cpp
 *
 *  Created on: 18 Jan 2017
 *      Author: walkingbeard
 */

#include "HandlerAttitudeEncoder.h"

HandlerAttitudeEncoder::HandlerAttitudeEncoder()
{
    // TODO Auto-generated constructor stub

}


void HandlerAttitudeEncoder::init()
{
    initEncoder();
}


void HandlerAttitudeEncoder::run()
{
    while(1) {
        TM_Motor encoderVal;
        encoderVal.value = readEncoder(tmInterval);
//        encoderVal.value = 4.0;
        topicTMMotor.publish(encoderVal);
        suspendCallerUntil(NOW() + tmInterval);
    }
}

