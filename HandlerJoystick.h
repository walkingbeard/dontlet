/*
 * HandlerJoystick.h
 *
 *  Created on: 5 Feb 2017
 *      Author: walkingbeard
 */

#ifndef HANDLERJOYSTICK_H_
#define HANDLERJOYSTICK_H_

#include <thread.h>
#include "subscriber.h"
#include "tmTcTopics.h"

#define JOYSTICK_MAX_VAL 150
#define MOTOR_MAX_VAL    1000

class HandlerJoystick: public RODOS::Thread, public RODOS::SubscriberReceiver<
        TC_JoystickValues> {
private:
    float curVal;

public:
         HandlerJoystick();
    void init();
    void run();
    void put(TC_JoystickValues &newVal);
};

#endif /* HANDLERJOYSTICK_H_ */
