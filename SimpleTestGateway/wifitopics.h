/*****************************************************************
topics.h

Original Created by: Atheel Redah @ University of W�rzburg
Original Creation Date: March 8, 2015

Development environment specifics:
	Software Platform: Rodos (Realtime Onboard Dependable Operating System).
	Hardware Platform: STM32F4 + W�rzburg Uni Informatik 8 Discovery AddOn Board Version 2.0.
*****************************************************************/

#ifndef __topics_h__
#define __topics_h_


/* Includes ------------------------------------------------------------------*/
#include "wifi_Interface/wf121.h"
#include "wifi_Interface/linkinterfacewf121.h"
#include "../tmTcTopics.h"
#define PACK_SIZE	50



//struct Telemetry1
//{
//	char ch[2];
//
//
//};
//
//
//struct Telemetry2
//{
//	int a,b;
//	float data[2];
//};
//
//
//struct Telecommand
//{
//	   float data;
//	    char id;
//
//};

//extern Topic<Telemetry1> telemetry1;
//extern Topic<Telemetry2> telemetry2;
//extern Topic<Telecommand> telecommand;

extern HAL_UART gatewayWifi; // USB-UART
extern WF121 wf121;
extern LinkinterfaceWF121 linkwf121;
extern Gateway gw;


#endif













