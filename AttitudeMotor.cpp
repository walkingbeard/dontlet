/*
 * AttitudeMotor.cpp
 *
 *  Created on: 3 Jan 2017
 *      Author: walkingbeard
 */

#include "AttitudeMotor.h"
#include "hBridge.h"

AttitudeMotor::AttitudeMotor() :
        hBridgeInA(HBRIDGE_B_INA_GPIO), hBridgeInB(HBRIDGE_B_INB_GPIO), motor(
                PWM_MOTOR_INDEX)
{
    motor.init(ATM_PWM_FREQ, ATM_PWM_INC);
    hBridgeInA.init(true, 1, 1);    // Is output, 1 pin, init high
    hBridgeInB.init(true, 1, 0);    // Is output, 1 pin, init low
    enableHBridges();
    motor.write(0); // Start motor at halt
}

void AttitudeMotor::setSpeed(int16_t speed)
{
    int16_t cycle = speed;
    if (speed > 500)
        speed = 500;
    if (speed < -500)
        speed = -500;

    PRINTF("Writing raw speed %d\n", speed);
    motor.write(speed / 10);
    if (speed < -30) {
        // Reverse direction
        hBridgeInA.setPins(0);
        hBridgeInB.setPins(1);
    } else if (speed > 30) {
        // Forward direction
        hBridgeInB.setPins(0);
        hBridgeInA.setPins(1);
    }
}
