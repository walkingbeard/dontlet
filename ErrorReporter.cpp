/*
 * ErrorReporter.cpp
 *
 *  Created on: 6 Jan 2017
 *      Author: walkingbeard
 */

#include "ErrorReporter.h"
#include "tmTcTopics.h"

ErrorReporter::ErrorReporter() :
        SubscriberReceiver<char *>(topicErrorReports, "Error reporter")
{

}


void ErrorReporter::put(char * &msg)
{
    PRINTF("%s\n", msg);
}
