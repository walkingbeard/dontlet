/*
 * HandlerThermalKnife.h
 *
 *  Created on: 6 Jan 2017
 *      Author: walkingbeard
 */

#ifndef HANDLERTHERMALKNIFE_H_
#define HANDLERTHERMALKNIFE_H_

#include <subscriber.h>
#include <thread.h>
#include "tmTcTopics.h"
#include "components.h"

class HandlerThermalKnife: public RODOS::Thread,
        public RODOS::SubscriberReceiver<TC_ThermalKnife> {
private:
    double  timeToRun;  // Time in seconds until switch-off

public:
    HandlerThermalKnife();
    void init();
    void run();
    void put(TC_ThermalKnife &runtime);
};

#endif /* HANDLERTHERMALKNIFE_H_ */
