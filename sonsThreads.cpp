/*
 * sonsThreads.cpp
 *
 *  Created on: 5 Jan 2017
 *      Author: walkingbeard
 */


#include "sonsThreads.h"

HandlerAttitudeMotor handlerAttitude;
HandlerThermalKnife  handlerKnife;
HandlerImu           handlerImu;
//ErrorReporter        errorReporter;
HandlerLightSensor   handlerLightSensor;
HandlerDockingGo     handlerDocking;
HandlerJoystick      handlerJoystick;
HandlerAttitudeEncoder handlerEncoder;
