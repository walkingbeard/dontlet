#include "rodos.h"
#include "math.h"
#include "lightSensor_Working.h"



#define TSL_ADDRESS1 0x39
#define TSL_ADDRESS2 0x49
#define TSL_CH0_LOW 0xAC


HAL_I2C i2c2(I2C_IDX1);  //(IDX3 SCL = PA8, SDA = PC9)


LIGHT::LIGHT(){

}

void LIGHT::init()
{

	int a1,a2;

	i2c2.init(100000);

				uint8_t cmd[2];
				uint8_t readback[1];

						cmd[0] = LIGHT_REG_CONTROL;

					//	cmd[0] = 0x80;
						cmd[1] = 0x03;
						a1=i2c2.write(LIGHT_I2C_ADRESS, cmd, 2);

						PRINTF("Initialization Status %d\n",a1);





						cmd[0] = LIGHT_COMMAND_BIT | LIGHT_REG_TIMING;
						cmd[1] = LIGHT_INTEGRATIONTIME_13MS ;
						a2=i2c2.write(LIGHT_I2C_ADRESS, cmd, 2);
						 PRINTF("Set Timing Status %d\n",a2);


						 //  HAL_I2C::writeRead(const uint8_t addr, const uint8_t* txBuf, uint32_t txBufSize, uint8_t* rxBuf, uint32_t rxBufSize) {


						i2c2.writeRead(LIGHT_I2C_ADRESS,&cmd[0],1,&readback[0],1);



}

void LIGHT::readLight(float *data) {


	//initialized_lightSensor();
	uint8_t i2cAddress;
	uint8_t cmd[2];
	uint8_t rx[4] = {0,0,0,0};
	int32_t retVal;
	cmd[0] = 0x9C;

	i2cAddress = LIGHT_I2C_ADRESS;

	retVal = i2c2.writeRead(i2cAddress, cmd, 1, rx, 4);
	PRINTF("The retval is =%d\n",retVal);

	 if(retVal<0){
		 i2c2.reset();
	     i2c2.init();
	 }

	 uint16_t channel0 = (rx[1] << 8) | rx[0];
	 uint16_t channel1 = (rx[3] << 8) | rx[2];

	 PRINTF("Channel 0: %x, Channel 1: %x\n", channel0, channel1);

	 	float lux = -1;
	 	float ratio = (channel0 == 0) ? 0 : (float)channel1 / channel0;
	 	if (ratio <= 0.5) {
	 		// lux = (0.0304 - 0.062 * pow(ratio, 1.4)) * channel0;
	 		// or:
	 		if (ratio <= 0.125) {
	 			lux = (0.0304 - 0.0272 * ratio) * channel0;
	 		}
	 		else if (ratio <= 0.250) {
	 			lux = (0.0325 - 0.0440 * ratio) * channel0;
	 		}
	 		else if (ratio <= 0.375) {
	 			lux = (0.0351 - 0.0544 * ratio) * channel0;
	 		}
	 		else {
	 			lux = (0.0381 - 0.0624 * ratio) * channel0;
	 		}
	 	}
	 	else if (ratio <= 0.61) {
	 		lux = (0.0224 - 0.031 * ratio) * channel0;
	 	}
	 	else if (ratio <= 0.80) {
	 		lux = (0.0128 - 0.0153 * ratio) * channel0;
	 	}
	 	else if (ratio <= 1.3){
	 		lux = (0.00146 - 0.00112 * ratio) * channel0;
	 	}
	 	else {
	 		lux = 0;
	 	}

	 	PRINTF("\n The LUX is =%f\n",lux);
	 	*data = lux;
}




void checkAndRecover(int32_t retVal) {
		if (retVal < 0) {
			switch (retVal) {
			case -1:
				xprintf("ERROR IMU I2C -1 HAL_I2C_ERR_START\n");
				break;
			case -2:
				xprintf("ERROR IMU I2C -2 HAL_I2C_ERR_STOP\n");
				break;
			case -3:
				xprintf("ERROR IMU I2C -3 HAL_I2C_ERR_WRITE\n");
				break;
			case -4:
				xprintf("ERROR IMU I2C -4 HAL_I2C_ERR_READ\n");
				break;
			case -5:
				xprintf("ERROR IMU I2C -5 HAL_I2C_ERR_ADDR\n");
				break;
			case -99:
				xprintf("ERROR IMU I2C -99 HAL_I2C_ERR_NOT_MASTER\n");
				break;
			default:
				xprintf("ERROR IMU I2C %d UNKNOWN\n", retVal);
				break;
			}
			i2c2.init();
		}
}

