/*
 * DockingMagnet.h
 *
 *  Created on: 5 Feb 2017
 *      Author: walkingbeard
 */

#ifndef DOCKINGMAGNET_H_
#define DOCKINGMAGNET_H_

#include "hBridge.h"

class DockingMagnet {
private:
    HAL_GPIO hBridgeInA;
    HAL_GPIO hBridgeInB;
    HAL_PWM  magnetPWM;
public:
    DockingMagnet();
    void switchOn();
    void switchOff();
};

#endif /* DOCKINGMAGNET_H_ */
